package com.domanderisposte.client.service;

import com.domanderisposte.client.model.Utente;

public interface ClientInterface {
	
	void isAdmin(String username);
	
	void isGiudice(String username);
	
	void login(String username, String password);
	
	void saveInDB(Utente utente);
	
	void clearDB();
	
	void loadFromDB(String nickname);
	
	void loadFromDB();
	
}
