package com.domanderisposte.client.service;

import com.domanderisposte.client.model.Utente;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface StandardServiceAsync {
	
	void isAdmin(String username, AsyncCallback callback);
	
	void isGiudice(String username, AsyncCallback callback);
	
	void login(String username, String password, AsyncCallback callback);
	
	void saveInDB(Utente utente, AsyncCallback callback);
	
	void clearDB(AsyncCallback callback);
	
	void loadFromDB(String nickname, AsyncCallback callback);
	
	void loadFromDB(AsyncCallback callback);
	
}
