package com.domanderisposte.client.service;

import com.domanderisposte.client.model.Utente;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("basicservice")
public interface StandardService extends RemoteService {
	
	boolean isAdmin(String username);
	
	boolean isGiudice(String username);
	
	boolean login(String username, String password);
	
	boolean saveInDB(Utente value);
	
	void clearDB();
	
	Utente loadFromDB(String nickname);
	
	String loadFromDB();

}
