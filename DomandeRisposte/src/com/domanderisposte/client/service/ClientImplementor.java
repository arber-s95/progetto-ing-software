package com.domanderisposte.client.service;

import com.domanderisposte.client.gui.MainGUI;
import com.domanderisposte.client.model.Utente;
import com.domanderisposte.client.service.ClientInterface;
import com.domanderisposte.client.service.StandardService;
import com.domanderisposte.client.service.StandardServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

public class ClientImplementor implements ClientInterface {
	
	private StandardServiceAsync serviceAsync;
	private MainGUI mainGUI;
	
	public ClientImplementor(String url) {
		serviceAsync = GWT.create(StandardService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) serviceAsync;
		endpoint.setServiceEntryPoint(url); // URL is the servlet url
				
		mainGUI = new MainGUI(this);
	}
	
	public MainGUI getMainGUI() {
		return mainGUI;
	}

	@Override
	public void isAdmin(String username) {
		serviceAsync.isAdmin(username, new AdminCallback());
	}
	
	@Override
	public void isGiudice(String username) {
		serviceAsync.isGiudice(username, new GiudiceCallback());
	}
	
	@Override
	public void login(String username, String password) {
		serviceAsync.login(username, password, new LoginCallback());
	}
	
	@Override
	public void saveInDB(Utente utente) {
		serviceAsync.saveInDB(utente, new SaveInDBCallback());
	}

	@Override
	public void clearDB() {
		serviceAsync.clearDB(new DefaultCallback());
	}
	
	@Override
	public void loadFromDB(String username) {
		serviceAsync.loadFromDB(username, new DefaultCallback());
	}
	
	@Override
	public void loadFromDB() {
		serviceAsync.loadFromDB(new DefaultCallback());
	}
	
	private class DefaultCallback implements AsyncCallback<Object> {

		@Override
		public void onFailure(Throwable caught) {
			System.out.println("È avvenuto un errore.");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof String)
				mainGUI.updateLabel((String) result);
			else if(result instanceof Utente)
				mainGUI.updateLabel(((Utente) result).getUsername() + " " + ((Utente) result).getEmail());
		}
		
	}
	
	private class SaveInDBCallback implements AsyncCallback<Object> {

		@Override
		public void onFailure(Throwable caught) {
			System.out.println("È avvenuto un errore.");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof Boolean)
				if((boolean) result)
					mainGUI.updateSuccessLabel("Nuovo utente registrato con successo.");
				else
					mainGUI.updateErrorLabel("Utente già presente.");
		}
		
	}

	private class LoginCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("È avvenuto un errore.");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof Boolean)
				if((boolean) result) {
					mainGUI.successLogin();
					mainGUI.displayMsg(MainGUI.SUCCESS_CODE, "Logged in.");
				} else
					mainGUI.displayMsg(MainGUI.ERROR_CODE, "Username e password non corrispondono.");
		}
	}
	
	private class AdminCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("È avvenuto un errore.");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof Boolean)
				if((boolean) result) {
					mainGUI.userIsAdmin();
					mainGUI.displayMsg(MainGUI.NEUTRAL_CODE, "Utente admin.");
				} else
					mainGUI.displayMsg(MainGUI.NEUTRAL_CODE, "Utente non amministratore.");
		}
	}
	
	private class GiudiceCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("È avvenuto un errore.");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof Boolean)
				if((boolean) result) {
					mainGUI.userIsGiudice();
					mainGUI.displayMsg(MainGUI.NEUTRAL_CODE, "Utente giudice.");
				} else
					mainGUI.displayMsg(MainGUI.NEUTRAL_CODE, "Utente non giudice.");
		}
	}
	
}
