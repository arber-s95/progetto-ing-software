package com.domanderisposte.client.gui;

import java.util.Date;

import com.domanderisposte.client.model.Utente;
import com.domanderisposte.client.service.ClientImplementor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DatePicker;

public class MainGUI extends Composite {
	
	public static final int SUCCESS_CODE = 0;
	public static final int ERROR_CODE = 1;
	public static final int NEUTRAL_CODE = 2;
	
	private VerticalPanel mainPanel = new VerticalPanel();
	private VerticalPanel navbarPanel = new VerticalPanel();
	private VerticalPanel contentPanel = new VerticalPanel();
	
	// REGISTRAZIONE
	private TextBox usernameTB_registrazione;
	private PasswordTextBox passwordTB_registrazione;
	private TextBox emailTB;
	
	private TextBox nomeTB;
	private TextBox cognomeTB;
	private ListBox sessoLB;
	private DatePicker dataNascitaTB;
	private TextBox luogoNascitaTB;
	private TextBox luogoDomicilioTB;
	private TextBox twitterTB;
	private TextBox instagramTB;
	
	private Label erroreLBL;
	private Label successoLBL;
	private Label infoLBL;
	private Label results;
	
	// LOGIN
	private TextBox usernameTB_login;
	private PasswordTextBox passwordTB_login;
	
	// Stato dell'utente
	private boolean isLogged = false;
	private boolean isAdmin = false;
	private boolean isGiudice = false;
	
	private ClientImplementor clientImpl;
	
	public MainGUI(ClientImplementor clientImpl) {
		initWidget(mainPanel);
		mainPanel.add(navbarPanel);
		mainPanel.add(contentPanel);
		
		this.clientImpl = clientImpl;
		
		setupNavbar();
	}
	
	public void displayMsg(int code, String msg) {
		switch(code) {
			case SUCCESS_CODE: 
				updateSuccessLabel(msg);
				break;
			case ERROR_CODE:
				updateErrorLabel(msg);
				break;
			case NEUTRAL_CODE:
				updateInfoLabel(msg);
				break;
		}
	}
	
	public void updateLabel(String value) {
		results.setText(value);
	}
	
	public void updateErrorLabel(String value) {
		erroreLBL.setText(value);
	}
	
	public void updateSuccessLabel(String value) {
		successoLBL.setText(value);
	}
	
	public void updateInfoLabel(String txt) {
		infoLBL.setText(txt);
	}
	
	private void resetLabels() {
		updateErrorLabel("");
		updateSuccessLabel("");
	}
	
	private void setupNavbar() {
		HorizontalPanel hPanel = new HorizontalPanel();
		
		Button registerBTN = new Button("Registrazione");
		registerBTN.setTitle("registrazione");
		registerBTN.addClickHandler(new ButtonClickHandler());
		hPanel.add(registerBTN);
		
		Button loginBTN = new Button("Login");
		loginBTN.setTitle("login");
		loginBTN.addClickHandler(new ButtonClickHandler());
		hPanel.add(loginBTN);
				
		Button logoutBTN = new Button("Logout");
		logoutBTN.setTitle("logout");
		logoutBTN.addClickHandler(new ButtonClickHandler());
		hPanel.add(logoutBTN);
		
		navbarPanel.add(hPanel);
	}
	
	private void setupRegistrazione() {
		contentPanel.clear();
		Grid gridPanel = new Grid(13, 2);
		
		// Text boxes
		Label lbl1 = new Label("Username: ");
		usernameTB_registrazione = new TextBox();
		usernameTB_registrazione.getElement().setPropertyString("placeholder", "Username");
		gridPanel.setWidget(0, 0, lbl1);
		gridPanel.setWidget(0, 1, usernameTB_registrazione);
		
		Label lbl2 = new Label("Password: ");
		passwordTB_registrazione = new PasswordTextBox();
		passwordTB_registrazione.getElement().setPropertyString("placeholder", "Password");
		gridPanel.setWidget(1, 0, lbl2);
		gridPanel.setWidget(1, 1, passwordTB_registrazione);
		
		Label lbl3 = new Label("Email: ");
		emailTB = new TextBox();
		emailTB.getElement().setPropertyString("placeholder", "Email");
		gridPanel.setWidget(2, 0, lbl3);
		gridPanel.setWidget(2, 1, emailTB);
		
		Label lbl4 = new Label("Nome: ");
		nomeTB = new TextBox();
		nomeTB.getElement().setPropertyString("placeholder", "Nome");
		gridPanel.setWidget(3, 0, lbl4);
		gridPanel.setWidget(3, 1, nomeTB);
						
		Label lbl5 = new Label("Cognome: ");
		cognomeTB = new TextBox();
		cognomeTB.getElement().setPropertyString("placeholder", "Cognome");
		gridPanel.setWidget(4, 0, lbl5);
		gridPanel.setWidget(4, 1, cognomeTB);
		
		Label lbl6 = new Label("Sesso: ");
		sessoLB = new ListBox();
		sessoLB.addItem("Maschio");
		sessoLB.addItem("Femmina");
		gridPanel.setWidget(5, 0, lbl6);
		gridPanel.setWidget(5, 1, sessoLB);
		
		Label lbl7 = new Label("Data di nascita");
		dataNascitaTB = new DatePicker();
		dataNascitaTB.setValue(new Date(), true);
		dataNascitaTB.getElement().setPropertyString("placeholder", "Data di nascita");
		gridPanel.setWidget(6, 0, lbl7);
		gridPanel.setWidget(6, 1, dataNascitaTB);
		
		Label lbl8 = new Label("Luogo di nascita: ");
		luogoNascitaTB = new TextBox();
		luogoNascitaTB.getElement().setPropertyString("placeholder", "Luogo di nascita");
		gridPanel.setWidget(7, 0, lbl8);
		gridPanel.setWidget(7, 1, luogoNascitaTB);
		
		Label lbl9 = new Label("Luogo di domicilio o residenza: ");
		luogoDomicilioTB = new TextBox();
		luogoDomicilioTB.getElement().setPropertyString("placeholder", "Luogo di domicilio");
		gridPanel.setWidget(8, 0, lbl9);
		gridPanel.setWidget(8, 1, luogoDomicilioTB);
		
		Label lbl10 = new Label("Username Twitter: ");
		twitterTB = new TextBox();
		twitterTB.getElement().setPropertyString("placeholder", "Username Twitter");
		gridPanel.setWidget(9, 0, lbl10);
		gridPanel.setWidget(9, 1, twitterTB);
		
		Label lbl11 = new Label("Username Instagram");
		instagramTB = new TextBox();
		instagramTB.getElement().setPropertyString("placeholder", "Username Instagram");
		gridPanel.setWidget(10, 0, lbl11);
		gridPanel.setWidget(10, 1, instagramTB);
		
		
		// Labels
		erroreLBL = new Label("");
		erroreLBL.setStylePrimaryName("error-label");
		successoLBL = new Label("");
		successoLBL.setStylePrimaryName("success-label");
		gridPanel.setWidget(11, 1, erroreLBL);
		gridPanel.setWidget(12, 1, successoLBL);
		
		// Buttons
		Button saveBTN = new Button("Salva");
		saveBTN.addClickHandler(new SaveBTNClickHandler());
		gridPanel.setWidget(11, 0, saveBTN);
		
		contentPanel.add(gridPanel);
		
		Button loadBTN = new Button("Carica");
		loadBTN.addClickHandler(new LoadBTNClickHandler());
		contentPanel.add(loadBTN);
		
		Button clearBTN = new Button("Cancella");
		clearBTN.addClickHandler(new ClearBTNClickHandler());
		contentPanel.add(clearBTN);
		
		results = new Label("--");
		contentPanel.add(results);
	}
	
	private void setupLogin() {
		contentPanel.clear();
		Grid gridPanel = new Grid(4, 2);
		
		// Text boxes
		Label username = new Label("Username: ");
		usernameTB_login = new TextBox();
		usernameTB_login.getElement().setPropertyString("placeholder", "Username");
		gridPanel.setWidget(0, 0, username);
		gridPanel.setWidget(0, 1, usernameTB_login);
		
		Label password = new Label("Password: ");
		passwordTB_login = new PasswordTextBox();
		passwordTB_login.getElement().setPropertyString("placeholder", "Password");
		gridPanel.setWidget(1, 0, password);
		gridPanel.setWidget(1, 1, passwordTB_login);
		
		// Labels
		erroreLBL = new Label("");
		erroreLBL.setStylePrimaryName("error-label");
		successoLBL = new Label("");
		successoLBL.setStylePrimaryName("success-label");
		gridPanel.setWidget(2, 1, erroreLBL);
		gridPanel.setWidget(3, 1, successoLBL);
		
		// Buttons
		Button loginBTN = new Button("Save");
		loginBTN.addClickHandler(new LoginBTNClickHandler());
		gridPanel.setWidget(2, 0, loginBTN);
		
		contentPanel.add(gridPanel);
		
		infoLBL = new Label("--");
		contentPanel.add(infoLBL);
	}
	
	private void setupLogout() {
		contentPanel.clear();
		
		Grid gridPanel = new Grid(4, 2);
		
		// Labels
		erroreLBL = new Label("");
		erroreLBL.setStylePrimaryName("error-label");
		successoLBL = new Label("");
		successoLBL.setStylePrimaryName("success-label");
		gridPanel.setWidget(0, 0, erroreLBL);
		gridPanel.setWidget(0, 1, successoLBL);
		
		contentPanel.add(gridPanel);
		
		infoLBL = new Label("--");
		contentPanel.add(infoLBL);
		
		if(isLogged) {
			isLogged = false;
			isAdmin = false;
			displayMsg(SUCCESS_CODE, "Logout effettato con successo.");
		} else {
			displayMsg(NEUTRAL_CODE, "Devi prima effettuare il login.");
		}			
		
	}
	
	public void successLogin() {
		isLogged = true;
		clientImpl.isAdmin(usernameTB_login.getText().trim());
		clientImpl.isGiudice(usernameTB_login.getText().trim());
	}
	
	public void userIsAdmin() {
		if(isLogged)
			isAdmin = true;
		else 
			isAdmin = false;
	}
	
	public void userIsGiudice() {
		if(isLogged)
			isGiudice = true;
		else 
			isGiudice = false;
	}
	
	private class ButtonClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			Button clicked = (Button) event.getSource();
			switch(clicked.getTitle()) {
				case "registrazione": 
					setupRegistrazione();
					break;
				case "login": 
					setupLogin();
					break;
				case "logout": 
					setupLogout();
					break;
			}
		}
	}
	
	private class SaveBTNClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			
			String username = usernameTB_registrazione.getText().trim();
			String password = passwordTB_registrazione.getText().trim();
			String email = emailTB.getText().trim();
						
			// Mostro gli errori
			if(username.equals("")) {
				updateErrorLabel("L'username è obbligatorio");
			} else if(password.equals("")) {
				updateErrorLabel("La password è obbligatoria");
			} else if(email.equals("")) {
				updateErrorLabel("L'email è obbligatoria");
			} else {
				String nome = nomeTB.getText().trim();
				String cognome = cognomeTB.getText().trim();
				String sesso = sessoLB.getSelectedItemText();
				String dataNascita = dataNascitaTB.getValue().toString();
				String luogoNascita = luogoNascitaTB.getText().trim();
				String luogoDomicilio = luogoDomicilioTB.getText().trim();
				String twitter = twitterTB.getText().trim();
				String instagram = instagramTB.getText().trim();
				
				Utente utente = new Utente(username, password, email);
				
				if(!nome.equals("")) {
					utente.setNome(nome);
				} 
				if(!cognome.equals("")) {
					utente.setCognome(cognome);
				} 
				if(!sesso.equals("")) {
					utente.setSesso(sesso);
				} 
				if(!dataNascita.equals("")) {
					utente.setDataNascita(dataNascita);
				} 
				if(!luogoNascita.equals("")) {
					utente.setLuogoNascita(luogoNascita);
				} 
				if(!luogoDomicilio.equals("")) {
					utente.setLuogoDomicilio(luogoDomicilio);
				} 
				if(!twitter.equals("")) {
					utente.setTwitter(twitter);
				}
				if(!instagram.equals("")) {
					utente.setInstagram(instagram);
				}
				
				clientImpl.saveInDB(utente);
			}
		}
	}
	
	private class LoadBTNClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			clientImpl.loadFromDB();
		}
	}
		
	private class ClearBTNClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			clientImpl.clearDB();
		}
	}
	
	private class LoginBTNClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			
			String username = usernameTB_login.getText().trim();
			String password = passwordTB_login.getText().trim();
			
			if(username.equals("")) {
				updateErrorLabel("Username richiesto.");
			} else if(password.equals("")) {
				updateErrorLabel("Password richiesta.");
			} else {
				clientImpl.login(username, password);
			}
			
		}
	}
	
}

