package com.domanderisposte.client.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utente implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private String email;
	
	private String nome;
	private String cognome;
	private String sesso;
	private String dataNascita;
	private String luogoNascita;
	private String luogoDomicilio;
	private String twitter;
	private String instagram;
	
	private boolean isAdmin = false;
	private boolean isGiudice = false;
	
	public Utente() {

	}
	
	public Utente(String username, String password, String email) {
		this.username = username;
		this.password = password;
		this.email = email;
	}
	
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public String getSesso() {
		return sesso;
	}

	public String getDataNascita() {
		return dataNascita;
	}

	public String getLuogoNascita() {
		return luogoNascita;
	}

	public String getLuogoDomicilio() {
		return luogoDomicilio;
	}

	public String getTwitter() {
		return twitter;
	}

	public String getInstagram() {
		return instagram;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public boolean isGiudice() {
		return isGiudice;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public void setSesso(String sesso) {
		this.sesso = sesso;
	}

	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}

	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	public void setLuogoDomicilio(String luogoDomicilio) {
		this.luogoDomicilio = luogoDomicilio;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public void setGiudice(boolean isGiudice) {
		this.isGiudice = isGiudice;
	}

	public static String MD5(String string) {
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
			m.update(string.getBytes(), 0, string.length());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return new BigInteger(1, m.digest()).toString(16);
	}
	
	@Override
	public String toString() {
		return username + " - " + email;
	}
	
}