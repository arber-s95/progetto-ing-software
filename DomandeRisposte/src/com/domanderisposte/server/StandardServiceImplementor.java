package com.domanderisposte.server;

import java.io.File;
import java.util.Map;

import javax.servlet.ServletContext;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.domanderisposte.client.model.Utente;
import com.domanderisposte.client.service.StandardService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class StandardServiceImplementor extends RemoteServiceServlet implements StandardService {

	@Override
	public boolean login(String username, String password) {
		Utente utente = loadFromDB(username);
		if(utente != null) {
			if(utente.getPassword().equals(password))
				return true;
			else
				return false;
		} else
			return false;
	}
	
	@Override
	public boolean isAdmin(String username) {
		Utente utente = loadFromDB(username);
		if(utente != null) {
			return utente.isAdmin();
		} else
			return false;
	}
	
	@Override
	public boolean isGiudice(String username) {
		Utente utente = loadFromDB(username);
		if(utente != null) {
			return utente.isGiudice();
		} else
			return false;
	}
	
	@Override
	public boolean saveInDB(Utente utente) {
		if(loadFromDB(utente.getUsername()) == null) {
			DB db = getDB();
			Map<Long, Utente> utenti = db.getTreeMap("utente");
			long hash = (long) utente.getUsername().hashCode();
			utenti.put(hash, utente);
			db.commit();
			return true;
		} else
			return false;	
	}

	@Override
	public void clearDB() {
		DB db = getDB();
		Map<Long, Utente> utenti = db.getTreeMap("utente");
		utenti.clear();
		db.commit();
	}

	@Override
	public Utente loadFromDB(String username) {
		DB db = getDB();
		Map<Long, Utente> utenti = db.getTreeMap("utente");
		long hash = (long) username.hashCode();
		if(utenti.containsKey(hash))
			return utenti.get(hash);
		else
			return null;
	}

	@Override
	public String loadFromDB() {
		DB db = getDB();
		Map<Long, Utente> utenti = db.getTreeMap("utente");
		String elements = "";
		for(Map.Entry<Long, Utente> utente : utenti.entrySet())
			elements += utente.getValue().toString() + " - ";
		return elements;
	}
	
	private DB getDB() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB db = (DB)context.getAttribute("DB");
			if(db == null) {
				db = DBMaker.newFileDB(new File("db")).closeOnJvmShutdown().make();
				context.setAttribute("DB", db);
			}
			return db;
		}
	}
	
}
